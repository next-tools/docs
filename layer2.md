# Layer 2

## Important registers

Register | Name          | Description
---------|---------------|------------------------
$12      | REG_L2_PAGE   | Layer 2 position in RAM
$13      | REG_L2_SHADOW | Layer 2 shadow screen position in RAM
$14      | REG_TRANS_COL | Transparent 8-bit colour for layer 2
$15      | REG_SPRITE_CTL| Determines video layer order
$16      | REG_L2_XLSB   | LSB of scrolling X offset
$17      | REG_L2_Y      | Scrolling Y offset
$18      | REG_CLIP_L2   | Sets clipping positions for Layer 2
$1C      | REG_CLIP_CTL  | Allows reading of clipping positions
$69      | REG_DISP_CTL  | Enables Layer 2
$70      | REG_L2_CTL    | Sets Layer 2 resolution and palette offset (for 4-bit mode)
$71      | REG_L2_XMSB   | MSB of scrolling X offset (for 320/640x256 modes)

## Important I/O ports

Port  | Name      | Description
------|-----------|-----------------------------
$123b | IO_LAYER2 | Allows memory banking, read/write access to ROM area and visibility

## Overview

## Use Cases

### Enabling Layer 2

To enable Layer 2, ensure that:

- REG_L2_PAGE are set up to the correct 16K bank indexes ($00-$2d (1MB) /$6d
  (2MB)).
- The correct layer order is set up using REG_SPRITE_CTL.
- Scrolling offsets and window clips are reset or set to what you wish.
- You have the correct resolution setting using REG_L2_CTL.

Finally, you can enable Layer 2 using REG_DISP_CTL or I/O port IO_LAYER.



