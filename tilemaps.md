# Tilemaps

## Important registers

Register | Name           | Description
---------|----------------|----------------------------------
$1b      | REG_CLIP_TMAP  | Defines clip window for tilemap layer.
$2f      | REG_TMAP_XMSB  | MSB of scroll X offset for tilemap.
$30      | REG_TMAP_XLSB  | LSB of scroll X offset for tilemap.
$31      | REG_TMAP_Y     | Scroll Y offset for tilemap.
$43      | REG_PAL_CTL    | Palette selector for tilemap.
$68      | REG_ULA_CTL    | Selects blending/stencil modes for ULA/tilemap.
$6b      | REG_TMAP_CTL   | Enable, size, palettes, # tiles etc.
$6c      | REG_TMAP_ATTR  | Default tile attribute.
$6e      | REG_TMAP_BASE  | Defines where the tilemap is stored.
$6f      | REG_TILES_BASE | Defines where the tiles are stored.

## Overview

## Use Cases

### Enabling tilemap mode

Set up REG_TMAP_CTL to enable it and set modes:

```text
                    7       6       5       4       3       2       1       0
                +-------+-------+-------+-------+-------+-------+-------+-------+
REG_TMAP_CTL    |  ENA  | 40/80 | ATTR  |  PAL  |       0       |  512  |  TOP  |
                +-------+-------+-------+-------+-------+-------+-------+-------+

                ENA     1 = Enable
                40/80   Number of tiles horizontally, 0 = 40; 1 = 80
                ATTR    1 = No attribute entry in tilemap, use REG_TMAP_ATTR
                PAL     0 = 1st palette, 1 = 2nd palette
                512     1 = Activate 512 tiles mode
                TOP     1 = Force tilemap above ULA
```

You might want to disable ULA as well using REG_ULA_CTL ($68).
