# Palettes

## Important registers

Register | Name          | Description
---------|---------------|------------------------
$40      | REG_PAL_INDEX | Sets the next palette index to read/write to
$41      | REG_PAL_VAL8  | Writes a RRRGGGBB colour to the current index
$44      | REG_PAL_VAL16 | Writes a RRRGGGBBB colour to the current index
$43      | REG_PAL_CTL   | Selects which palettes to enable and to edit
$6b      | REG_TMAP_CTL  | Selects which palettes to enable for tilemaps

## Overview

A palette is a collection of 256 colours, each colour chosen from a set of 512
unique colours.  For each colour there are 8 levels of red, green and blue and
this gives us the 512 possible colours.

The Next supports multiple palettes for different systems in its hardware.  The
are four main video systems on the Next and each one has access to 2 different
palettes.  This allows a graphics system to suddenly switch to a different set
of 256 colours immediately.  The four main graphic systems are ULA, Layer 2,
Tilemap (or Layer 3) and Sprites.

## Use Cases

### Selecting the palettes for each video system

Each video system (ULA, Layer 2, Tilemap and Sprites) has access to two different palettes.  Selecting which is a matter of setting the correct bit on the appropriate register.  Read the register, set the bit and write the register back.

Below is a table of which register and bits to toggle for each graphics system.  To choose the 1st palette, set to 0, to choose the 2nd palette, set to 1.

Register       | Bit | Graphics System
---------------|-----|----------------------
REG_PAL_CTL    | 1   | ULA
REG_PAL_CTL    | 2   | Layer 2
REG_PAL_CTL    | 3   | Sprites
REG_TMAP_CTL   | 4   | Tilemaps
