# Sprites

## Important registers

Register | Name          | Description
---------|---------------|------------------------
$09      | REG_PERIPH_4  | Allows sprite lockstep mode
$15      | REG_SPRITE_CTL| Determines video layer order
$19      | REG_CLIP_SPR  | Manages window clipping of the sprite layer
$34      | REG_SPR_IDX   | Selects sprite index for other registers
$35      | REG_SPR_0     | X LSB for sprite
$36      | REG_SPR_1     | Y LSB for sprite
$37      | REG_SPR_2     | Palette offset, mirror, rotation
$38      | REG_SPR_3     | Visibility, use of REG_SPR_4 and pattern index
$39      | REG_SPR_4     | Special sprite features
$4b      | REG_SPR_TRANS | Palette index for transparency colour
$75      | REG_SPR_0_INC | As REG_SPR_0 but with auto-increment of sprite index
$76      | REG_SPR_1_INC | As REG_SPR_1 but with auto-increment of sprite index
$77      | REG_SPR_2_INC | As REG_SPR_2 but with auto-increment of sprite index
$78      | REG_SPR_3_INC | As REG_SPR_3 but with auto-increment of sprite index
$79      | REG_SPR_4_INC | As REG_SPR_4 but with auto-increment of sprite index

## Important I/O ports

Port  | Name           | Description
------|----------------|-----------------------------
$303b | IO_SPRITE      | Chooses the sprite index for editing
$xx57 | IO_SPRITE_ATTR | Write here to upload bytes to sprite attribute memory
$xx5b | IO_SPRITE_PATT | Write here to upload bytes to sprite patten memory

## Overview

## Sprite attributes

```text
          7    6    5    4    3    2    1    0
        +----+----+----+----+----+----+----+----+
ATTR 0  |                X position             |
        +----+----+----+----+----+----+----+----+
ATTR 1  |                Y position             |
        +----+----+----+----+----+----+----+----+
ATTR 2  | Palette Offset    | XM | YM | RT |  ? | ? = X8(anchor), rel pal
        +----+----+----+----+----+----+----+----+
ATTR 3  | EN | A4 |     Pattern Index (0-63)    |
        +----+----+----+----+----+----+----+----+
ATTR 4  | 4B | N6 | ?  | X Scale | Y Scale | Y8 |
        +----+----+----+----+----+----+----+----+

Composite sprites:
        +----+----+----+----+----+----+----+----+
ATTR 4  | 0  | 1  | N6 | X Scale | Y Scale | RP |   RP = Relative pattern
        +----+----+----+----+----+----+----+----+

Big sprites:
        +----+----+----+----+----+----+----+----+
ATTR 4  | 0  | 1  | N6 | 0  | 0  | 0  | 0  | RP |   RP = Relative pattern
        +----+----+----+----+----+----+----+----+

```

## Use Cases

### Setting the current sprite

By setting the sprite index you declare which sprite pattern or attribute you
wish to edit via other IO ports or registers.  This can either be done via port
IO_SPRITE, or by REG_SPR_IDX.  If you use a port, then ports must be used to
upload data, unless the lockstep mode has been selected.  Likewise, if you use
a register to set the index, then all uploads must be done via registers,
unless the lockstep mode has been selected.

Lockstep mode is enabled by setting bit 4 of REG_PERIPH_4.  This means any
write to IO_SPRITE is the same as writing to REG_SPR_IDX and vice versa.

When a sprite is selected, you can write attributes 0 to 4 in order via IO port
IO_SPRITE_ATTR, or out of order via the REG_SPR_0 to REG_SPR_4 registers.
Also, you can write patterns via the IO_SPRITE_PATT port.  There is no next
register for uploading patterns.  This means that unless you have lockstep mode
on, you must use an IO port to set the sprite index to upload patterns.

### Uploading Sprite Patterns

- Set the sprite index via IO port (IO_SPRITE) or if lockstep is enabled, via a
  nextreg.
- OUT the 256 (256 colour sprites) or 128 bytes (16-colour sprites) to
  IO_SPRITE_PATT.

